import * as React from 'react';
import { MenuItem } from 'app/types';
import utils from 'app/helpers/utils';

const menus: MenuItem[] = [
    {
        icon: 'fas fa-tachometer-alt',
        text: 'Dashboard',
        description: '',
        link: utils.link('admin/dashboard'),
    },
];

export default menus;
