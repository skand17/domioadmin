import { Dashboard, Markdown, DataTableExample } from 'app/pages/admin';
import { NotFound } from 'app/pages/public';
import utils from 'app/helpers/utils';

const routes = [
    {
        path: utils.link('admin/dashboard'),
        exact: true,
        component: Dashboard,
    },
    {
        component: NotFound,
    },
];

export default routes;
